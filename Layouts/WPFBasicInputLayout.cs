﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using XAppFramework;
using XAppFramework.Reflection;
using XAppFramework.Actions;

namespace XAppFramework.WPF
{
    public class WPFBasicInputLayout : IBasicInputLayout
    {
        public event OnValueUpdated ValueChanged = delegate { };

        StackPanel _panel;
        IGenericInputControl _genericControl;


        public bool IsDirty
        {
            get
            {

                return _genericControl.IsDirty;
            }
        }

        public WPFBasicInputLayout(IGenericInputControl control)
        {
            _genericControl = control;
            _genericControl.ValueChanged += _genericControl_ValueChanged;
        }

        void _genericControl_ValueChanged(object value)
        {
            ValueChanged(value);
        }

        public void CreateLayout()
        {
            if (_panel != null)
                _panel.Children.Clear();

            StackPanel inputStack = new StackPanel();
            inputStack.MinWidth = 200;
            inputStack.Children.Add(new Label() { Content = _genericControl.Name });

            Control ctrl = (Control)_genericControl.GetControl();

            IStyleProvider<Control> ctrl_style = XApp.CreateFor<IBasicInputLayout, IStyleProvider<Control>>();
            ctrl_style.ApplyStyle(ctrl);

            inputStack.Children.Add(ctrl);

            _panel = inputStack;
        }

        public object GetControl()
        {
            return _panel;
        }

        public bool IsEnabled
        {
            get
            {
                Control c = (Control)_genericControl.GetControl();
                return c.IsEnabled;
            }
            set
            {
                _genericControl.IsEnabled = value;
            }
        }


        public void SetValue(object o)
        {
            _genericControl.SetValue(o);
        }

        public object GetValue()
        {
            return _genericControl.GetValue();
        }


        public void Focus()
        {
            _genericControl.Focus();
        }
    }

}
