﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using XAppFramework;
using XAppFramework.Reflection;
using XAppFramework.Actions;

namespace XAppFramework.WPF
{
    public class WPFListInputLayout : IListInputLayout
    {
        public event OnValueUpdated ValueChanged = delegate { };

        public bool IsExpanded
        {
            get
            {
                return _list.IsExpanded;
            }
            set
            {
                _list.IsExpanded = value;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _list.IsEnabled;
            }
            set
            {
                _list.IsEnabled = value;

                foreach (IGenericInputLayout o in _children)
                    o.IsEnabled = value;
            }
        }

        TypeAssemblyInfo _listItemType;
        TypeAssemblyInfo _listType;
        List<IGenericInputLayout> _children;
        IGenericInputLayout _parent;
        Expander _list;
        StackPanel _listPanel;

        public WPFListInputLayout(IGenericInputLayout parentLayout, TypeAssemblyInfo list_type)
        {
            _parent = parentLayout;
            _children = new List<IGenericInputLayout>();
            _listType = list_type;

            if (list_type.IsArray)
            {
                TypeAssemblyInfo arrayItemType = list_type.ElementType;
                _listItemType = arrayItemType;
            }
            else
            {
                List<TypeAssemblyInfo> generic_args = list_type.GenericArguments;
                if (generic_args.Count > 0)
                {
                    TypeAssemblyInfo innerType = generic_args[0];
                    _listItemType = innerType;
                }
                else
                {
                    throw new System.ArgumentException("Cannot create a list from enumerable type: " + list_type);
                }
            }
        }

        public void CreateLayout()
        {
            Expander expander = new Expander();
            StackPanel innerPanel = new StackPanel();
            innerPanel.Margin = new Thickness() { Left = 15 };

            expander.Header = _parent.Name;
            expander.Content = innerPanel;

            innerPanel.Children.Add(new Label() { Content = _parent.Name });

            _listPanel = new StackPanel();
            _listPanel.Margin = new Thickness() { Left = 15 };

            innerPanel.Children.Add(_listPanel);

            StackPanel buttonPanel = new StackPanel();
            innerPanel.Children.Add(buttonPanel);

            buttonPanel.Orientation = Orientation.Horizontal;
            Button addBtn = new Button();
            addBtn.Content = "Add " + _parent.Name.TrimEnd('s');
            addBtn.Click += addBtn_Click;

            Button removeBtn = new Button();
            removeBtn.Content = "Remove " + _parent.Name.TrimEnd('s');
            removeBtn.Click += removeBtn_Click;

            buttonPanel.Children.Add(addBtn);
            buttonPanel.Children.Add(removeBtn);

            IStyleProvider<Control> ctrl_style = XApp.CreateFor<IListInputLayout, IStyleProvider<Control>>();
            ctrl_style.ApplyStyle(expander);

            _list = expander;
        }


        public object GetControl()
        {
            return _list;
        }


        public void SetValue(object o)
        {
            if (o == null) return;

            SerializableObject list = (SerializableObject)o;
            List<SerializableObject> obj_list = list.Fields;

            int i = 0;
            foreach (SerializableObject item in obj_list)
            {
                if (i < _children.Count)
                {
                    //if (_children[i].Type.Name != item.Type.Name)
                    //{
                    //    _children[i].SetType(item.Type);
                    //}
                    _children[i].SetValue(item);
                }
                else
                {
                    IGenericInputLayout new_child = add_listItem(item.Type);
                    new_child.SetValue(item);
                    new_child.ValueChanged += new_child_ValueChanged;
                }
                i++;
            }

            for (int k = i; k < _children.Count; ++k)
            {
                _listPanel.Children.RemoveAt(k);
                _children.RemoveAt(k);
                k--;
            }
        }

        void new_child_ValueChanged(object value)
        {
            ValueChanged(value);
        }

        public object GetValue()
        {
            SerializableObject list_obj = new SerializableObject();
            list_obj.Type = _listType;
            list_obj.IsList = true;
            list_obj.Fields = new List<SerializableObject>();
            list_obj.Name = _parent.Name;

            list_obj.IsNull = false;

            List<object> objs = new List<object>();

            int idx = 0;
            foreach (IGenericInputLayout child in _children)
            {
                object child_data = child.GetValue();

                if (child_data is SerializableObject)
                {
                    list_obj.Fields.Add((SerializableObject)child_data);
                }
                else
                {
                    list_obj.Fields.Add(SerializableObject.Serialize(child_data, list_obj.Name + "[" + idx + "]"));
                }
                idx++;
            }

            return list_obj;

        }

        void remove_listItem()
        {
            if (_listPanel.Children.Count > 0)
            {
                _listPanel.Children.RemoveAt(_listPanel.Children.Count - 1);
                _children.RemoveAt(_children.Count - 1);
            }

        }

        void removeBtn_Click(object sender, RoutedEventArgs e)
        {
            remove_listItem();
        }

        void addBtn_Click(object sender, RoutedEventArgs e)
        {
            add_listItem(_listItemType, true);
        }

        IGenericInputLayout add_listItem(TypeAssemblyInfo t, bool expand = false)
        {

            StackPanel itemPanel = new StackPanel();
            itemPanel.Margin = new Thickness() { Left = 15 };

            itemPanel.MouseRightButtonUp += itemPanel_MouseRightButtonUp;

            StackPanel namePanel = new StackPanel();
            namePanel.Orientation = Orientation.Horizontal;
            namePanel.Children.Add(new Label() { Content = "Item " + _children.Count });

            IGenericInputLayout new_child = XApp.Create<IGenericInputLayout, string, TypeAssemblyInfo>(_parent.Name, t); //new DTLayoutObj(_parent.Name, t, _parent.OnControlValueChanged);
            itemPanel.Children.Add((UIElement)new_child.GetControl());
            itemPanel.Children.Add(new Separator());

            _children.Add(new_child);
            _listPanel.Children.Add(itemPanel);

            if (expand)
                new_child.IsExpanded = true;

            if (new_child.HasValue())
            {
                MenuItem delete_item = new MenuItem()
                {
                    Header = "Delete '" + new_child.Name + "'",
                    Command = new ActionCommand(XApp.Create<IDeleteListItemAction, IGenericInputLayout, int>(_parent, _children.Count - 1))
                };

                MenuItem move_up_item = new MenuItem()
                {
                    Header = "Move Up",
                    Command = new ActionCommand(XApp.Create<IMoveListItemAction, IGenericInputLayout, int, bool>(_parent, _children.Count - 1, true))
                };

                MenuItem move_down_item = new MenuItem()
                {
                    Header = "Move Down",
                    Command = new ActionCommand(XApp.Create<IMoveListItemAction, IGenericInputLayout, int, bool>(_parent, _children.Count - 1, false))
                };

                itemPanel.ContextMenu = new ContextMenu();
                itemPanel.ContextMenu.Items.Add(move_up_item);
                itemPanel.ContextMenu.Items.Add(move_down_item);
                itemPanel.ContextMenu.Items.Add(delete_item);
            }


            return new_child;
        }

        void itemPanel_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            StackPanel panel = (StackPanel)sender;

            // This is dumb, but this seems to be an easy way to get CanExecute to run again..
            // by rebuilding the menu. -_-
            ContextMenu new_menu = new ContextMenu();

            for (int i = 0; i < panel.ContextMenu.Items.Count; ++i)
            {
                MenuItem item = (MenuItem)panel.ContextMenu.Items[i];

                ActionCommand cmd = (ActionCommand)item.Command;
                ActionCommand new_cmd = new ActionCommand(cmd);

                panel.ContextMenu.Items.RemoveAt(i);


                new_menu.Items.Add(new MenuItem()
                {
                    Header = item.Header,
                    Command = new_cmd
                });
                i--;
            }

            panel.ContextMenu = new_menu;
            panel.ContextMenu.IsOpen = true;
        }

        public void Focus()
        {
            _list.Focus();

            if (_children != null && _children.Count > 0)
            {
                _children[0].Focus();
            }
        }


        public bool IsDirty
        {
            get
            {
                for (int i = 0; i < _children.Count; ++i)
                {
                    if (_children[i].IsDirty)
                        return true;
                }

                return false;
            }
        }
    }

}
