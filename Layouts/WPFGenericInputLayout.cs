﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using XAppFramework;
using XAppFramework.Reflection;


namespace XAppFramework.WPF
{
    public class WPFGenericInputLayout : IGenericInputLayout
    {       
        public string Name { get { return _name; } }
        public TypeAssemblyInfo Type { get { return _type; } }

        public event OnValueUpdated ValueChanged = delegate { };

        public bool IsDirty
        {
            get
            {
                return _layout.IsDirty;
            }
        }

        public bool IsExpanded
        {
            get
            {
                if (_layout is IListInputLayout)
                {
                    IListInputLayout layout = (IListInputLayout)_layout;
                    return layout.IsExpanded;
                }
                else if (_layout is IObjectInputLayout)
                {
                    IObjectInputLayout layout = (IObjectInputLayout)_layout;
                    return layout.IsExpanded;
                }

                return false;
            }
            set
            {
                if (_layout is IListInputLayout)
                {
                    IListInputLayout layout = (IListInputLayout)_layout;
                    layout.IsExpanded = value;
                }else if (_layout is IObjectInputLayout)
                {
                    IObjectInputLayout layout = (IObjectInputLayout)_layout;
                    layout.IsExpanded = value;
                }
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _layout.IsEnabled;
            }
            set
            {
                _layout.IsEnabled = value;
            }
        }

        string _name;
        TypeAssemblyInfo _type;
        TypeAssemblyInfo _innerType;

        List<System.Attribute> _attributes;
        StackPanel _listPanel;

        StackPanel _masterPanel;
        System.Action _onChange;

        IInputLayout _layout;
        object _valueRef;

        public WPFGenericInputLayout(FieldAssemblyInfo f)
        {
            IAssemblyQueryer ass_queryer = XApp.Create<IAssemblyQueryer>();
            IAttributeQueryer attr_queryer = XApp.Create<IAttributeQueryer>();
            //IDTEApplication app = XApp.GetSingleton<IDTEApplication>();
            //ActiveAssemblyInterface ass_interface = app.Project.ActiveAssembly;

            // Get the desired name for this field, not necessarily what reflection says it is
            string name = ass_queryer.GetFieldName(f); //ass_interface.GetFieldName(f);

            InputLayoutAttrArgs attr_args = new InputLayoutAttrArgs();

            attr_args.PopulateFromType = attr_queryer.FindFirst(new AttributeQuery()
            {
                FieldName = name,
                TypeName = f.DeclaringTypeName
            });//ass_interface.GetAutoPopulateType(f.DeclaringTypeName, f.Name);

            InitLayout(f.Name, f.FieldType, attr_args);
        }

        public WPFGenericInputLayout(string name, TypeAssemblyInfo type, InputLayoutAttrArgs attr_args = null)
        {
            InitLayout(name, type, attr_args);
        }

        public WPFGenericInputLayout(string name, TypeAssemblyInfo type)
        {
            InitLayout(name, type, null);
        }

        void InitLayout(string name, TypeAssemblyInfo type, InputLayoutAttrArgs attr_args = null)
        {
            _attributes = new List<Attribute>();
            _masterPanel = new StackPanel();

            //_field = field;
            _name = name;
            _type = type;//_field.FieldType;

            TypeAssemblyInfo auto_populate_type = attr_args != null ? attr_args.PopulateFromType : null;

            if (attr_args != null && attr_args.FixedPopulateNames != null && attr_args.FixedPopulateValues != null)
            {
                throw new System.NotImplementedException("Nope!");
            }
            else if (auto_populate_type != null)
            {
                IGenericInputControl input_ctrl = XApp.Create<IGenericInputControl, string, TypeAssemblyInfo, TypeAssemblyInfo>(name, _type, auto_populate_type);
                //WPFGenericInputControl control = new WPFGenericInputControl(name, _type, auto_populate_type, OnControlValueChanged);
                _layout = XApp.Create<IBasicInputLayout, IGenericInputLayout, IGenericInputControl>(this, input_ctrl);
                _layout.ValueChanged += _layout_ValueChanged;
            }
            else
                if (_type.IsBasic)
                {
                    IGenericInputControl control = XApp.Create<IGenericInputControl, string, TypeAssemblyInfo>(name, _type);
                    _layout = XApp.Create<IBasicInputLayout, IGenericInputControl>(control);
                    _layout.ValueChanged += _layout_ValueChanged;
                }
                else if (_type.IsEnumerable)
                {
                    _layout = XApp.Create<IListInputLayout, IGenericInputLayout, TypeAssemblyInfo>(this, _type);
                    _layout.ValueChanged += _layout_ValueChanged;
                }
                else
                {
                    _layout = XApp.Create<IObjectInputLayout, IGenericInputLayout, TypeAssemblyInfo>(this, _type);
                    _layout.ValueChanged += _layout_ValueChanged;
                }

            CreateLayout();

        }

        void _layout_ValueChanged(object value)
        {
            _valueRef = value;
            ValueChanged(value);
        }

        public bool HasAttribute<T>()
            where T : System.Attribute
        {
            for (int i = 0; i < _attributes.Count; ++i)
            {
                if (_attributes[i] is T)
                    return true;
            }
            return false;
        }


        public bool HasValue()
        {
            return _valueRef != null;
        }

        public bool HasError()
        {
            return false;
        }

        public string GetErrorMessage()
        {
            return "";
        }

        public void FlagForError(string message)
        {

        }

        public void ClearError()
        {

        }

        public void Disable()
        {
            _layout.IsEnabled = false;
        }

        public void SetType(TypeAssemblyInfo t)
        {
            _type = t;
        }

        public void SetValue(object o)
        {
            _layout.SetValue(o);
        }


        public object GetValue()
        {
            return _layout.GetValue();
        }

        public void Focus()
        {
            _layout.Focus();
        }

        void create_MasterPanel()
        {
            if (_masterPanel != null)
                _masterPanel.Children.Clear();

            _masterPanel = new StackPanel();
            _masterPanel.Children.Add((UIElement)_layout.GetControl());
        }

        public void CreateLayout()
        {
            _layout.CreateLayout();
        }

        public object GetControl()
        {
            create_MasterPanel();

            return _masterPanel;
        }

    }
}
