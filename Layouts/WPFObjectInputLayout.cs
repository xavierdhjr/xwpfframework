﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using XAppFramework;
using XAppFramework.Reflection;
using XAppFramework.Actions;

namespace XAppFramework.WPF
{
    public class WPFObjectInputLayout : IObjectInputLayout
    {
        public event OnValueUpdated ValueChanged = delegate { };

        TypeAssemblyInfo _objType;
        List<IGenericInputLayout> _children;
        IGenericInputLayout _parent;
        Expander _obj;
        string _name;


        public bool IsDirty
        {
            get
            {
                for (int i = 0; i < _children.Count; ++i)
                {
                    if (_children[i].IsDirty)
                        return true;
                }

                return false;
            }
        }

        public WPFObjectInputLayout(IGenericInputLayout parent, TypeAssemblyInfo obj_type)
        {
            _parent = parent;
            _children = new List<IGenericInputLayout>();
            _objType = obj_type;


            List<FieldAssemblyInfo> fields = obj_type.Fields;

            foreach (FieldAssemblyInfo f in fields)
            {
                IGenericInputLayout generic_layout = XApp.Create<IGenericInputLayout, FieldAssemblyInfo>(f);
                generic_layout.ValueChanged += generic_layout_ValueChanged;
                _children.Add(generic_layout);

                ILayoutAttributeApplier attr_applier = XApp.Create<ILayoutAttributeApplier>();
                attr_applier.ApplyAttributes(f, _children[_children.Count - 1]);
            }
        }

        void generic_layout_ValueChanged(object value)
        {
            ValueChanged(value);
        }

        public void CreateLayout()
        {
            List<FieldAssemblyInfo> fields = _objType.Fields;

            Expander expander = new Expander();

            StackPanel innerPanel = new StackPanel();
            innerPanel.Margin = new Thickness() { Left = 15 };

            foreach (IGenericInputLayout obj in _children)
            {
                innerPanel.Children.Add((UIElement)obj.GetControl());
            }

            expander.Header = _parent.Name;
            _name = _parent.Name;
            expander.Content = innerPanel;
            expander.IsExpanded = true;

            _obj = expander;
        }

        public object GetControl()
        {
            return _obj;
        }


        public bool IsExpanded
        {
            get
            {
                return _obj.IsExpanded;
            }
            set
            {
                _obj.IsExpanded = value;
            }
        }


        public bool IsEnabled
        {
            get
            {
                return _obj.IsEnabled;
            }
            set
            {
                _obj.IsEnabled = value;

                foreach (IGenericInputLayout o in _children)
                    o.IsEnabled = value;
            }
        }


        public void SetValue(object o)
        {
            if (o == null)
                return;

            SerializableObject obj = (SerializableObject)o;

            string name = obj.Name;
            if (!string.IsNullOrEmpty(name))
            {
                _name = obj.Name.Replace("_", "__");
            }

            _obj.Header = _name;

            List<SerializableObject> obj_fields = obj.Fields;

            for (int i = 0; i < _objType.Fields.Count; ++i)
            {
                XApp.ASSERT(() => _objType.Fields[i].Name == obj_fields[i].Name, XAppFailure.UI_DATA_DISPLAY,
                    "Field type mismatch. Field " + _objType.Fields[i].Name + " != " + obj_fields[i].Name);
                   

                object val = null;

                if (obj_fields[i].IsList)
                {
                    val = obj_fields[i].Fields;
                }
                else
                {
                    val = obj_fields[i].Value;
                }
                _children[i].SetValue(val);

            }
        }

        public object GetValue()
        {
            SerializableObject obj = new SerializableObject();
            obj.Type = _objType;
            obj.IsNull = false;
            obj.Fields = new List<SerializableObject>();
            obj.Name = _name;

            for (int i = 0; i < _objType.Fields.Count; ++i)
            {
                object child_data = _children[i].GetValue();
                SerializableObject field_obj = SerializableObject.Serialize(child_data, _objType.Fields[i].Name);
                field_obj.Type = _objType.Fields[i].FieldType;

                obj.Fields.Add(field_obj);
            }

            return obj;
        }


        public void Focus()
        {
            _obj.Focus();
        }
    }
}
