﻿using System;
using System.Windows;

namespace XAppFramework.WPF
{
    public class WPFNewFileDialog : INewFileDialog
    {
        public bool ShowNewFileDialog(DialogParams parameters, Action<string> onFileCreated)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.InitialDirectory = parameters.InitialDirectory;
            dlg.FileName = parameters.DefaultFileName; //"New Table"; // Default file name
            dlg.DefaultExt = parameters.DefaultExtension; // Default file extension
            dlg.Filter = parameters.Filter; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                onFileCreated(dlg.FileName);
            }

            return result.HasValue ? result.Value : false;
        }
    }

}
