﻿using System;
using System.Windows;

namespace XAppFramework.WPF
{
    public class WPFSaveFileDialog : ISaveFileDialog
    {
        public bool ShowSaveFileDialog(DialogParams parameters, Action<string> onFileSaved)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            dlg.FileName = parameters.DefaultFileName;
            dlg.InitialDirectory = parameters.InitialDirectory;
            dlg.DefaultExt = parameters.DefaultExtension;
            dlg.FileName = parameters.Filter;

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                onFileSaved(dlg.FileName);
            }

            return false;
        }
    }

}
