﻿using System;
using System.Windows;

namespace XAppFramework.WPF
{
    public class WPFOpenDialog : IOpenDialog
    {
        public bool ShowOpenFileDialog(string initial_directory, System.Action<string> onFileSelected)
        {
            return ShowOpenFileDialog(new DialogParams()
            {
                InitialDirectory = initial_directory
            }, onFileSelected);
        }

        public bool ShowOpenFilesDialog(string initial_directory, System.Action<string[]> onFilesSelected)
        {
            return ShowOpenFilesDialog(new DialogParams()
            {
                InitialDirectory = initial_directory
            }, onFilesSelected);
        }


        public bool ShowOpenFileDialog(DialogParams parameters, System.Action<string> onFileSelected)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.InitialDirectory = parameters.InitialDirectory;

            dlg.FileName = parameters.DefaultFileName;
            dlg.DefaultExt = parameters.DefaultExtension; // Default file extension
            dlg.Filter = parameters.Filter; // Filter files by extension

            bool? open_file_result = dlg.ShowDialog();

            if (open_file_result == true)
            {
                onFileSelected(dlg.FileName);
            }

            return open_file_result.HasValue ? open_file_result.Value : false;
        }

        public bool ShowOpenFilesDialog(DialogParams parameters, System.Action<string[]> onFilesSelected)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.InitialDirectory = parameters.InitialDirectory;

            dlg.FileName = parameters.DefaultFileName;
            dlg.DefaultExt = parameters.DefaultExtension; // Default file extension
            dlg.Filter = parameters.Filter; // Filter files by extension

            bool? open_file_result = dlg.ShowDialog();

            if (open_file_result == true)
            {
                onFilesSelected(dlg.FileNames);
            }

            return open_file_result.HasValue ? open_file_result.Value : false;
        }
    }

}
