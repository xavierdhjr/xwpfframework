﻿using System;
using System.Windows;

namespace XAppFramework.WPF
{
    public class WPFNotificationDialog : INotificationDialog
    {
        public void ShowDialog(string title, string body)
        {
            System.Windows.MessageBox.Show(body, title, MessageBoxButton.OK);
        }
    }

}
