﻿using System;
using System.Windows;

namespace XAppFramework.WPF
{
    public class WPFYesNoDialog : IYesNoDialog
    {
        public void ShowDialog(string title, string body, System.Action on_yes, System.Action on_no)
        {
            var result = System.Windows.MessageBox.Show(body, title, MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                on_yes();
            }
            else
            {
                on_no();
            }
        }
    }

}
