﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAppFramework.WPF
{
    public class WPFBindings : IIoCBinder
    {
        public void BindAllIoCClasses()
        {
            XApp.BindIoC<INotificationDialog, WPFNotificationDialog>();
            XApp.BindIoC<IYesNoDialog, WPFYesNoDialog>();
            XApp.BindIoC<IOpenDialog, WPFOpenDialog>();
            XApp.BindIoC<INewFileDialog, WPFNewFileDialog>();

            XApp.BindIoC<IInputListControl, WPFListInput>();
            XApp.BindIoC<IInputValueControl<int>, WPFIntInput>();
            XApp.BindIoC<IInputValueControl<bool>, WPFBoolInput>();
            XApp.BindIoC<IInputValueControl<float>, WPFFloatInput>();
            XApp.BindIoC<IInputValueControl<string>, WPFStringInput>();
            XApp.BindIoC<IInputValueControl<uint>, WPFUIntInput>();

            XApp.BindIoC<IBasicInputLayout, WPFBasicInputLayout>();
            XApp.BindIoC<IListInputLayout, WPFListInputLayout>();
            XApp.BindIoC<IObjectInputLayout, WPFObjectInputLayout>();

            XApp.BindIoC<IGenericInputControl, WPFGenericInputControl>();
            XApp.BindIoC<IGenericInputLayout, WPFGenericInputLayout>();
        }
    }
}
