﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace XAppFramework.WPF
{
    public class WPFIntInput : IInputValueControl<int>
    {
        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }

            set
            {
                _control.IsEnabled = value;
            }
        }

        public event OnValueUpdated<int> ValueChanged;

        Xceed.Wpf.Toolkit.IntegerUpDown _control;
        int _value;
        bool _isDirty;
        int _prevValue;

        public WPFIntInput()
        {
            _control = new Xceed.Wpf.Toolkit.IntegerUpDown();
            _control.ValueChanged += _control_ValueChanged;

            _control.AutoSelectBehavior = Xceed.Wpf.Toolkit.AutoSelectBehavior.OnFocus;
        }

        private void _control_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (_prevValue != GetValue())
                _isDirty = true;
            ValueChanged(GetValue());
        }

        public void Focus()
        {
            _control.Focus();
        }

        public void ClearValue()
        {
            _prevValue = 0;
            _isDirty = false;
            _control.Value = 0;
        }

        public int GetValue()
        {
            return _control.Value.HasValue ? _control.Value.Value : 0;
        }

        public void SetValue(int value)
        {
            _prevValue = value;
            _control.Value = value;
            _isDirty = false;
        }

        public object GetControl()
        {
            return _control;
        }

        public void SetValue(object o)
        {
            if (o is string)
                SetValue(int.Parse((string)o));
            else
                SetValue((int)o);
        }
    }

}
