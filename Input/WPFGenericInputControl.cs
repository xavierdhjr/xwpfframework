﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using XAppFramework.Reflection;
using XAppFramework;


namespace XAppFramework.WPF
{
    public class WPFGenericInputControl : IGenericInputControl
    {
        public const string COMBOBOX_EMPTY_OPTION = "(None)";

        public string Name { get { return _name; } }
        public bool IsDirty
        {
            get
            {
                XApp.ASSERT(() => _control != null, XAppFailure.UI_DATA_DISPLAY, "Control has been destroyed or was not created."); 
                return _control.IsDirty;
            }
        }
        public bool HasError { get { return _error; } }

        public string ErrorMessage { get { return _errorMsg; } }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }
            set
            {
                _control.IsEnabled = value;
            }
        }

        public bool IsExpanded
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public event OnValueUpdated ValueChanged;

        string _name;

        TypeAssemblyInfo _type;
        object _value;
        TypeAssemblyInfo _populateType;
        List<string> _populateDisplayValues;
        List<object> _populateValues;

        IInputValueControl _control;
        IPopulatedTypeQueryer _typeQueryer;
        IEnumQueryer _enumQueryer;

        bool _fixedPopulatedValues;
        bool _error;
        bool _valueWasSetOnce;
        string _errorMsg;

        public WPFGenericInputControl(string name, TypeAssemblyInfo dataType)
        {
            _name = name;
            _type = dataType;
            _populateType = null;
            setup_control_for_type();
        }

        public WPFGenericInputControl(string name, TypeAssemblyInfo dataType, TypeAssemblyInfo autoPopulateType)
        {
            _name = name;
            _type = dataType;
            _populateType = null;
            _populateType = autoPopulateType;
            setup_control_for_type();

        }

        public void FlagForError(string message)
        {
            _error = true;
            _errorMsg = message;
        }

        public void ClearError()
        {
            _error = false;
            _errorMsg = "";
        }

        void ValueChangedInternal()
        {
            ValueChanged(_value);
        }

        public void Enable()
        {
            _control.IsEnabled = true;
        }

        public void Disable()
        {
            _control.IsEnabled = false;
        }

        void setup_control_for_type()
        {

            if (_fixedPopulatedValues)
            {
                IInputListControl ctrl = XApp.Create<IInputListControl>();

                foreach (string display_value in _populateDisplayValues)
                {
                    ctrl.AddItem(display_value);
                }

                ctrl.ValueChanged += _list_ValueChanged;
                _control = ctrl;
            }
            else if(_populateType != null)
            {
                _typeQueryer = XApp.Create<IPopulatedTypeQueryer>();

                IInputListControl ctrl = XApp.Create<IInputListControl>();

                List<string> results = _typeQueryer.FindAll(_populateType);

                //List<DTEditorLib.Data.LoadedTableObject> objs = _app.Project.GetObjectsOfType(_populateType); //MainWindow.Get.GetObjectsOfType(_populateType);

                ctrl.AddItem(COMBOBOX_EMPTY_OPTION);
                _value = null;

                foreach (string str in results)
                {
                    ctrl.AddItem(str);
                }
                
                ctrl.ValueChanged += _list_ValueChanged;
                _control = ctrl;
            }
            else if (_type.IsEnum)
            {
                _enumQueryer = XApp.Create<IEnumQueryer>();

                IInputListControl ctrl = XApp.Create<IInputListControl>();

                //string[] enum_names = _app.Project.ActiveAssembly.GetEnumNames(_type.Name); //System.Enum.GetNames(_type);
                List<string> enum_names = _enumQueryer.FindAll(_type);

                foreach (string item in enum_names)
                {
                    ctrl.AddItem(item);
                }

                ctrl.ValueChanged += _list_EnumValueChanged;

                _control = ctrl;
            }
            else if (_type == typeof(bool))
            {
                IInputValueControl<bool> ctrl = XApp.Create<IInputValueControl<bool>>();
                ctrl.ValueChanged += _bool_ValueChanged;

                _control = ctrl;
            }
            else if (_type == typeof(string))
            {
                IInputValueControl<string> ctrl = XApp.Create<IInputValueControl<string>>();
                ctrl.ValueChanged += _string_ValueChanged;
                _control = ctrl;
            }
            else if (_type == typeof(int))
            {
                IInputValueControl<int> ctrl = XApp.Create<IInputValueControl<int>>();
                ctrl.ValueChanged += _int_ValueChanged;
                _control = ctrl;
            }
            else if (_type == typeof(uint))
            {
                IInputValueControl<uint> ctrl = XApp.Create<IInputValueControl<uint>>();
                ctrl.ValueChanged += _uint_ValueChanged;
                _control = ctrl;
            }
            else if (_type == typeof(float))
            {
                IInputValueControl<float> ctrl = XApp.Create<IInputValueControl<float>>();
                ctrl.ValueChanged += _float_ValueChanged;
                _control = ctrl;
            }
            else
            {
                XApp.ASSERT(() => false, XAppFailure.UI_DATA_DISPLAY, "Invalid type provided: " + _type);
            }

            
        }

        void _uint_ValueChanged(uint value)
        {
            _value = value;
            ValueChangedInternal();
        }

        private void _list_EnumValueChanged(string value)
        {
            _value = _enumQueryer.ParseEnum(_type.Name, value); //_app.Project.ActiveAssembly.ParseEnum(_type.Name, value);
            ValueChangedInternal();
        }

        private void _float_ValueChanged(float value)
        {
            _value = value;
            ValueChangedInternal();
        }

        private void _int_ValueChanged(int value)
        {
            _value = value;
            ValueChangedInternal();
        }

        private void _string_ValueChanged(string value)
        {
            _value = value;
            ValueChangedInternal();
        }

        private void _bool_ValueChanged(bool value)
        {
            _value = value;
            ValueChangedInternal();
        }

        private void _list_ValueChanged(string value)
        {
            IInputListControl list = (IInputListControl)_control;

            if (_populateType != null)
            {
                if (list.SelectedIndex == 0)
                {
                    _value = "";
                }
                else
                {
                    List<string> results = _typeQueryer.FindAll(_populateType);
                    //List<DTEditorLib.Data.LoadedTableObject> objs = _app.Project.GetObjectsOfType(_populateType); //MainWindow.Get.GetObjectsOfType(_populateType);
                    _value = results[list.SelectedIndex - 1];
                }
            }
            else if (_fixedPopulatedValues)
            {
                _value = _populateValues[list.SelectedIndex];
            }
            ValueChangedInternal();
        }

        public void ClearValue()
        {
            if (_control == null)
                throw new System.InvalidOperationException("Invalid control");
            _control.ClearValue();
        }

        public object GetValue() { return _value; }

        public void SetValue(object o)
        {
            if (o == null)
            {
                ClearValue();
                return;
            }

            _value = o;

            if(_value is SerializableObject)
            {
                SerializableObject obj = (SerializableObject)_value;
                _value = obj.Value;
            }

            /*
            switch(_controlType)
            {
                case POPULATED:
            */
            if (_control is IInputListControl)
            {
                IInputListControl list = (IInputListControl)_control;

                if (_value == null)
                {
                    list.SelectedIndex = 0;
                }
                else if(_type.IsEnum)
                {
                    string enum_name = _enumQueryer.GetEnumName(_type.Name, _value); //_app.Project.ActiveAssembly.GetEnumName(_type.Name, _value);
                    _control.SetValue(enum_name);
                }
                else if (_populateType != null)
                {
                    // Ex. Populate from type
                    //List<DTEditorLib.Data.LoadedTableObject> objs = _app.Project.GetObjectsOfType(_populateType); //MainWindow.Get.GetObjectsOfType(_populateType);

                    List<string> results = _typeQueryer.FindAll(_populateType);

                    for (int i = 0; i < results.Count; ++i)
                    {

                        if (XApp.string_hash(results[i]) == XApp.string_hash((string)o))
                        {
                            list.SelectedIndex = i + 1;
                            break;
                        }
                    }

                }
                else if (_fixedPopulatedValues)
                {
                    // Ex. Populate from constants
                    for (int i = 0; i < _populateValues.Count; ++i)
                    {
                        if (_populateValues[i].Equals(o))
                        {
                            list.SelectedIndex = i;
                            break;
                        }
                    }
                }
            }
            else
            {
                _control.SetValue(o);
            }

            _valueWasSetOnce = true;
        }

        public void Focus()
        {
            if (_control == null)
                throw new System.InvalidOperationException("Invalid control");
            _control.Focus();
        }


        public object GetControl()
        {
            if (_control == null)
                throw new System.InvalidOperationException("Invalid control");

            return (Control)_control.GetControl();
        }

    }
}
