﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace XAppFramework.WPF
{
    public class WPFListInput : IInputListControl
    {
        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }

            set
            {
                _control.IsEnabled = value;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return _control.SelectedIndex;
            }

            set
            {
                _control.SelectedIndex = value;
            }
        }

        public event OnValueUpdated<string> ValueChanged;

        ComboBox _control;
        bool _isDirty;

        int _prevValue;

        public WPFListInput()
        {
            _control = new ComboBox()
            {
                Margin = new Thickness(0, 0, 0, 5)
            };
            _control.IsEditable = true;
            _control.SelectionChanged += _control_SelectionChanged;
        }

        private void _control_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_control.SelectedIndex == -1)
                return;

            if (_prevValue != _control.SelectedIndex)
                _isDirty = true;
            ValueChanged(GetValue());
        }

        public void Focus()
        {
            _control.Focus();
        }

        public void AddItem(string item)
        {
            _control.Items.Add(item);
        }

        public void ClearValue()
        {
            _prevValue = 0;
            _isDirty = false;
            _control.SelectedIndex = 0;
        }

        public string GetValue()
        {
            string value = (string)_control.Items[_control.SelectedIndex];
            return value;
        }

        public void SetValue(string value)
        {
            _isDirty = false;
            for(int i = 0; i < _control.Items.Count; ++i)
            {
                if(_control.Items[i].ToString() == value)
                {
                    _prevValue = i;
                    SelectedIndex = i;
                }
            }
        }

        public object GetControl()
        {
            return _control;
        }


        public void SetValue(object o)
        {
            if (o == null)
            {
                _control.SelectedIndex = 0;
                return;
            }

            SetValue(o.ToString());
        }
    }
}
