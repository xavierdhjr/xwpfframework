﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace XAppFramework.WPF
{
    public class WPFStringInput : IInputValueControl<string>
    {
        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }

            set
            {
                _control.IsEnabled = value;
            }
        }

        public event OnValueUpdated<string> ValueChanged;

        TextBox _control;
        bool _isDirty;
        string _prevValue;

        public WPFStringInput()
        {
            _control = new TextBox();
            _control.TextChanged += _control_TextChanged;
        }

        private void _control_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (GetValue() != _prevValue)
                _isDirty = true;
            ValueChanged(GetValue());
        }

        public void Focus()
        {
            _control.Focus();
            _control.SelectAll();
        }

        public void ClearValue()
        {
            _prevValue = null;
            _isDirty = false;
            _control.Text = null;
        }

        public string GetValue()
        {
            return _control.Text;
        }

        public void SetValue(string value)
        {
            _prevValue = value;
            _isDirty = false;
            _control.Text = value;
        }

        public object GetControl()
        {
            return _control;
        }

        public void SetValue(object o)
        {
            SetValue((string)o);
        }
    }

}
