﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace XAppFramework.WPF
{
    public class WPFBoolInput : IInputValueControl<bool>
    {
        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }

            set
            {
                _control.IsEnabled = value;
            }
        }

        public event OnValueUpdated<bool> ValueChanged;

        CheckBox _control;
        bool _isDirty;
        bool _prevValue;

        public WPFBoolInput()
        {
            _control = new CheckBox();
            _control.Checked += _control_Checked;
            _control.Unchecked += _control_Unchecked;
        }

        private void _control_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_prevValue != false)
                _isDirty = true;
            ValueChanged(GetValue());
        }

        private void _control_Checked(object sender, RoutedEventArgs e)
        {
            if (_prevValue != true)
                _isDirty = true;
            ValueChanged(GetValue());
        }

        public void Focus()
        {
            _control.Focus();
        }

        public void ClearValue()
        {
            _prevValue = false;
            _isDirty = false;
            _control.IsChecked = false;
        }

        public bool GetValue()
        {
            return _control.IsChecked.HasValue ? _control.IsChecked.Value : false;
        }

        public void SetValue(bool value)
        {
            _prevValue = value;
            _isDirty = false;
            _control.IsChecked = value;
        }

        public object GetControl()
        {
            return _control;
        }

        public void SetValue(object o)
        {
            if (o is string)
                SetValue(bool.Parse((string)o));
            else
                SetValue((bool)o);
        }
    }

}
