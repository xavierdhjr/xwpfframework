﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace XAppFramework.WPF
{
    public class WPFFloatInput : IInputValueControl<float>
    {
        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }

            set
            {
                _control.IsEnabled = value;
            }
        }

        public event OnValueUpdated<float> ValueChanged;

        Xceed.Wpf.Toolkit.SingleUpDown _control;
        float _value;
        bool _isDirty;

        float _prevValue;

        public WPFFloatInput()
        {
            _control = new Xceed.Wpf.Toolkit.SingleUpDown();
            _control.ValueChanged += _control_ValueChanged;
            _control.AutoSelectBehavior = Xceed.Wpf.Toolkit.AutoSelectBehavior.OnFocus;
        }

        private void _control_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (_prevValue != GetValue())
                _isDirty = true;
            ValueChanged(GetValue());
        }

        public void Focus()
        {
            _control.Focus();
        }

        public void ClearValue()
        {
            _prevValue = 0;
            _control.Value = 0;
            _isDirty = false;
        }

        public float GetValue()
        {
            return _control.Value.HasValue ? _control.Value.Value : 0;
        }

        public void SetValue(float value)
        {
            _prevValue = value;
            _control.Value = value;
            _isDirty = false;
        }

        public object GetControl()
        {
            return _control;
        }

        public void SetValue(object o)
        {
            if (o is string)
                SetValue(float.Parse((string)o));
            else
                SetValue((float)o);
        }
    }

}
