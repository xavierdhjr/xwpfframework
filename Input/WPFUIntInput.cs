﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace XAppFramework.WPF
{
    public class WPFUIntInput : IInputValueControl<uint>
    {
        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _control.IsEnabled;
            }

            set
            {
                _control.IsEnabled = value;
            }
        }

        public event OnValueUpdated<uint> ValueChanged;

        Xceed.Wpf.Toolkit.LongUpDown _control;
        int _value;
        bool _isDirty;
        uint _prevValue;

        public WPFUIntInput()
        {
            _control = new Xceed.Wpf.Toolkit.LongUpDown();
            _control.ValueChanged += _control_ValueChanged;

            _control.AutoSelectBehavior = Xceed.Wpf.Toolkit.AutoSelectBehavior.OnFocus;
        }

        private void _control_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (_prevValue != GetValue())
                _isDirty = true;
            ValueChanged(GetValue());
        }

        public void Focus()
        {
            _control.Focus();
        }

        public void ClearValue()
        {
            _prevValue = 0;
            _isDirty = false;
            _control.Value = 0;
        }

        public uint GetValue()
        {
            return _control.Value.HasValue ? (uint)_control.Value : 0;
        }

        public void SetValue(uint value)
        {
            _prevValue = value;
            _isDirty = false;
            _control.Value = value;
        }

        public object GetControl()
        {
            return _control;
        }


        public void SetValue(object o)
        {
            if (o is string)
            {
                SetValue(uint.Parse((string)o));
            }
            else
                SetValue((uint)o);
        }
    }

}
